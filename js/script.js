// object, method, id
let calculatorMemory = document.getElementById("screen");
// cell or screen
console.log(calculatorMemory);
console.log(typeof(calculatorMemory));
let evalFlag = true;
let saveValueToMemory = '0';
const OPERATORS = ['+', '-', '*', '/']
const OPERATORS_DECPOINT = ['+', '-', '*', '/', '.']
// keypad (keydown) values for *, +, -, ., /
const KEY_CODES =[106, 107, 109, 110, 111, 190];

function putOnScreen(aValue) {
    // check if digit can fit on the screen (maximum 19)
    if (calculatorMemory.value.length > 18) {
        evalFlag = false;
        calculatorMemory.value = '0';
        alert("Error. Too many digits!");
        return false;
    }
        // at start when 0 or result on screen
        if (calculatorMemory.value === '0' || evalFlag) {
            // if numbers follow, replace zero (no leading zero) using includes method
            if (!OPERATORS_DECPOINT.includes(aValue)) {
                calculatorMemory.value = aValue;
            // if operators/dec. point follow, keep zero or value on the screen
            } else {
                calculatorMemory.value += aValue;
            }
        // this part checks for two operators/decimal point one after another
        } else if (OPERATORS_DECPOINT.includes(aValue)) {
            let aLength = calculatorMemory.value.length;
            if (aLength > 0 && OPERATORS_DECPOINT.includes(calculatorMemory.value[aLength-1])) {
                return alert("One operator is just enough.");
            } else {
                calculatorMemory.value += aValue;
            }
        // numbers
        } else {
            calculatorMemory.value += aValue;
        }
        console.log("inside putOnScreen", calculatorMemory.value);
        evalFlag = false;
        return true;
}

function calculateResult() {
    console.log("before", calculatorMemory.value);
    // eliminates extra operator
    if (OPERATORS.includes(calculatorMemory.value[calculatorMemory.value.length-1])) {
        // slice string method, return new string: from zero to second last
        calculatorMemory.value = calculatorMemory.value.slice(0, -1);
    }
    // try if everything good; throw error if problem
    try {
        // eval function transforms string to result
        calculatorMemory.value = eval(calculatorMemory.value);
        console.log("after", calculatorMemory.value);
        evalFlag = true;
    }
    catch (error) {
        alert("Error. Probably too many dots.");
        return false;
    }
    return true;
}

function calculatorReset() {
    calculatorMemory.value = '0';
    evalFlag = false;
    return true;
}

function backwards() {
    // two characters or more, cut the last
    if (calculatorMemory.value.length > 1) {
        calculatorMemory.value = calculatorMemory.value.slice(0, -1);
    } else {
        calculatorMemory.value = 0;
    }
    return true;
}

function memoryFunction(aValue) {
    switch (aValue) {
        case 'm':
            if (calculatorMemory.value !== '0') {
                // place to memory
                saveValueToMemory = calculatorMemory.value;
                document.getElementById("screen").style.background = "papayawhip";
            } else {
                // erase from memory
                saveValueToMemory = '0';
                document.getElementById("screen").style.background = "white";
            }
            evalFlag = true;
            break;
        // return from memory on to screen
        case 'rm':
            if (saveValueToMemory !== '0') {
                if (OPERATORS.includes(calculatorMemory.value[calculatorMemory.value.length-1])) {
                    calculatorMemory.value += eval(saveValueToMemory);
                } else {
                    calculatorMemory.value = eval(saveValueToMemory);
                }
            } else {
                alert("There is nothing in memory.");
                return false;
            }
            break;
    }
    return true;
}

// https://stackoverflow.com/questions/12153357/how-to-register-document-onkeypress-event
// addEventListener method with keydown event (HTMLS DOM events)
document.addEventListener("keydown", checkKeyDown);

// https://www.w3.org/2002/09/tests/keys.html
// only space, number and letter keys have keycodes correlating to String.fromCharCode method as it uses Unicode values
function checkKeyDown(e) {
    let aChar = '';
    console.log("event", e);
    console.log("keyboard code", e.keyCode);
    if ((e.keyCode > 95 && e.keyCode < 106) || (e.keyCode > 47 && e.keyCode < 58)) {
        if (e.keyCode > 94) {
            aChar = String.fromCharCode(e.keyCode-48);
        } else {
            aChar = String.fromCharCode(e.keyCode);
        }
        console.log(aChar);
        console.log(typeof(aChar));
        putOnScreen(aChar);
    } else if (KEY_CODES.includes(e.keyCode)) {
        aChar = String.fromCharCode(e.keyCode-64);
        console.log(aChar);
        console.log(typeof(aChar));
        putOnScreen(aChar);
    } else if (e.keyCode === 187 || e.keyCode === 13) {
        calculateResult();
    } else if (e.keyCode === 39) {
        backwards();        
    } else if (e.keyCode === 67) {
        calculatorReset();
    } else if (e.keyCode === 77) {
        memoryFunction('m');
    } else if (e.keyCode === 82) {
        memoryFunction('rm');
    }
    return true;
}