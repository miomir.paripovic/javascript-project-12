# Project no. 12

## Task

This is my assignment for week no. 12 and my second JavaScript project. The main goal for this week is to create a calculator with basic functions: addition, multiplication, divison, and subtraction. To make it more challenging I decided to add extra functionalities:

- add to memory
- return to memory
- clear the screen
- backwards
- ability to enter values with both mouse and keyboard